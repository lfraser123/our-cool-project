#!/usr/bin/env python3
# Jay Miles,Luke Fraser, Mia Carroll
# Cellular Automata Project : CSE 40932
# Oct 4, 2020
# Main driver program. General overview:

# User can run program with any number of the following flags : -init [initial values] -rand -gen -rule -life -type -default

# Initial values must be a sequence of 0s and 1s, separated by commas. Each sequence represents a row and thus the number of 0s and 1s
# must equal the value of the width(default 10).  User uses a 1 to indicate those cells which are alive to start, 0 for those
# that are dead. Unspecified rows will be dead. If the -init flag is not on, then the initial state is randomized, and height and width
# are 100 (for 2D, width=1 for 1D).

# Default rule is B2/S01, corresponding to Wolfram rule 30. User can use flag -life to use game of life rules (or default).

import sys
import numpy as np
from cellular_automata.cellular_automata import CellularAutomata as ca
TYPE = '2D'
INIT_STATE = np.zeros(shape=(10, 10))
RULE = 'B2/S01'
NUM_GEN = 100
LIFE = True
INIT_BOOL = False
SAVE = False
FILENAME = ''

# functions


def usage():
    print('''
        Usage: ./main.py [-init <list of inital values> | -rand] 
        [-gen <number of generations>] [-rule <Bxxx/Sxxx> | -life]
        [-type <1D | 2DG | 2DT>] [-default]
        [-save filename] 
        Default: 2D Totalistic Game of Life. 100 Generations. 100x100
        Grid. Random Initial State. 
    ''')


def main():

    # error check
    # print(sys.argv[2])
    if len(sys.argv) < 2:
        usage()
        exit(1)

    # parse command line
    arguments = sys.argv[1:]
    while arguments and arguments[0].startswith('-'):
        argument = arguments.pop(0)
        if argument == '-init':
            global INIT_BOOL
            INIT_BOOL = True
            i = 0
            while arguments[0].startswith('-') == False:
                init = arguments.pop(0).split(',')
                global INIT_STATE
                INIT_STATE[i] = init
                i = i + 1
        elif argument == '-gen':
            global NUM_GEN
            NUM_GEN = int(arguments.pop(0))
        elif argument == '-rule':
            global RULE
            RULE = arguments.pop(0)
            global LIFE
            LIFE = False
        elif argument == '-type':
            global TYPE
            TYPE = arguments.pop(0)
        elif argument == '-life':
            # global LIFE
            LIFE = True
        elif argument == '-save':
            global SAVE
            global FILENAME
            SAVE = True
            FILENAME = arguments.pop(0)

       # set dimensions
    if TYPE == '1D':
        width = 1
    else:
        width = 100
    height = 100

    if INIT_BOOL == False:
        input_arr = ''
    else:
        input_arr = INIT_STATE

    # setup machine (call constructor), run once
    my_automata = ca(width=width, height=height,
                     input_array=input_arr, generations=NUM_GEN,
                     save_image=SAVE, filename=FILENAME)
    if LIFE == False and TYPE != '2DG':
        my_automata.specify_rule_2d(RULE)
    elif TYPE == '2DG':
        my_automata.diagonal()
    else:
        my_automata.game_of_life()
    pass


if __name__ == '__main__':
    main()
